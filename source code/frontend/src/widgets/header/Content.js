import React, { Component } from "react";
import Container from "../../core/Container";

class Header extends Component {
  render() {
    return <Container>Quarkit</Container>;
  }
}

export default Header;
