import QuarkChain from "../../core/quarkchain-web3/dist/index";
const basicToken = require("./BasicTokenImpl.json");
const pausableToken = require("./PausableTokenImpl.json");
const burnableToken = require("./BurnableTokenImpl.json");
const pausableAndBurnableToken = require("./PausableBurnableTokenImpl.json");
const crowdsale = require("./CrowdsaleImpl.json");

const web3 = window.web3;

export function tokenChoise(pausable, burnable) {
  let token = pausable
    ? burnable
      ? pausableAndBurnableToken
      : pausableToken
    : burnable
      ? burnableToken
      : basicToken;
  return token;
}

export async function deploy(values, props) {
  if (!web3.qkc) {
    QuarkChain.injectWeb3(web3, "http://jrpc.testnet.quarkchain.io:38391");
  }
  const ethAddr = web3.eth.accounts[0];
  if (ethAddr) {
    const qkcAddr = QuarkChain.getQkcAddressFromEthAddress(ethAddr);

    //Token
    const token = tokenChoise(values.pausable, values.burnable);
    let Contract = web3.qkc.contract(token.abi);
    let SaleContract = web3.qkc.contract(crowdsale.abi);

    let contractOverride;
    Contract.new(
      qkcAddr.slice(0, -8),
      {
        data: token.bytecode,
        gas: 10000000,
        gasPrice: 10000000000
      },
      function(err, contractOverride) {
        const tProps = props;
        const tokenContract = contractOverride;
        console.log(contractOverride);
        setTimeout(() => {
          const tokenTransactionReceipt = web3.qkc.getTransactionReceipt(
            tokenContract.transactionHash
          );
          console.log(tokenTransactionReceipt);
          let saleContract;
          SaleContract.new(
            new Date().getTime(),
            new Date().getTime() + 604800000,
            "3",
            "10000",
            qkcAddr.slice(0, -8),
            tokenTransactionReceipt.contractAddress,
            {
              data: crowdsale.bytecode,
              gas: 10000000,
              gasPrice: 10000000000
            },
            (err, contractOverride) => {
              saleContract = contractOverride;
              const sProps = tProps;
              console.log(saleContract);
              setTimeout(() => {
                const crowdsaleTransactionReceipt = web3.qkc.getTransactionReceipt(
                  saleContract.transactionHash
                );
                console.log(crowdsaleTransactionReceipt);
                sProps.history.push(
                  `/${crowdsaleTransactionReceipt.contractAddress}/auth`
                );
              }, 45000);
            }
          );
        }, 30000);
      }
    );
  }
}
