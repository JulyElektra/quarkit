import React, { Component } from "react";
import Container from "../../core/Container";
import styled from "styled-components";
import AInput from "../../core/Input";

const Wrap = styled.div`
  margin: 0 auto;
  width: 300px;
`;

const Title = styled.div`
  font-size: 24px;
  margin-bottom: 30px;
`;

const Subtitle = styled.div`
  font-size: 16px;
  opacity: 0.4;
`;

const Input = styled(AInput)`
  margin-bottom: 20px;
  width: 100%;
`;

const Button = styled.button`
  margin-bottom: 20px;
  height: 42px;
  width: 120px;
  border-radius: 5px;
  font-size: 14px;
  background-color: #0098ff;
  color: #fff;
`;

class Content extends Component {
  state = {
    username: "",
    password: ""
  };

  componentDidMount() {}

  auth(crowdsaleId, setUser) {
    fetch(`http://163.172.139.34:8080/crowdsales/${crowdsaleId}/users`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        id: this.state.username,
        password: this.state.password
      })
    }).then(res => {
      setUser(res);
    });
  }

  render() {
    const { setUser } = this.props;
    return (
      <Container>
        <Wrap>
          <Title>Sign In</Title>
          <Subtitle>E-mail</Subtitle>
          <Input
            placeholder="example@gmail.com"
            value={this.state.username}
            onChange={event => this.setState({ username: event.target.value })}
          />
          <Subtitle>Password</Subtitle>
          <Input
            type="password"
            value={this.state.password}
            onChange={event => this.setState({ password: event.target.value })}
          />
          <Button
            onClick={() =>
              this.auth(this.props.match.params.crowdsaleId, setUser)
            }
          >
            Sign In
          </Button>
          <Subtitle>Don’t have an account?</Subtitle>
          <Button
            onClick={() =>
              this.auth(this.props.match.params.crowdsaleId, setUser)
            }
          >
            Sign Up
          </Button>
        </Wrap>
      </Container>
    );
  }
}

export default Content;
