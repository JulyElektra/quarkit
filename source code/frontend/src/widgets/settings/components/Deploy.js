import React, { Component } from "react";
import styled from "styled-components";
import { deploy, tokenChoise } from "../../quarkchain/Content";

const saleContract = require("../../quarkchain/CrowdsaleImpl.json");

const Container = styled.div``;

const Title = styled.div`
  font-size: 20px;
  margin-bottom: 10px;
`;

const Pre = styled.pre`
  position: relative;
  overflow: auto;
  padding: 1.5em;
  font-family: Consolas, Monaco, "Andale Mono", "Ubuntu Mono", monospace;
  white-space: pre;
  word-spacing: normal;
  word-break: normal;
  word-wrap: normal;
  hyphens: none;
  border-radius: 0.5em;
  background-color: rgba(71, 134, 255, 0.1);
  margin: 0;
  max-height: 200px;
  margin-bottom: 30px;
`;

const Code = styled.code`
  white-space: pre;
  word-spacing: normal;
  word-break: normal;
  hyphens: none;
  margin: 0;
  padding: 0;
`;

class Deploy extends Component {
  render() {
    const { pausable, burnable } = this.props.values;
    const token = tokenChoise(pausable, burnable);
    return (
      <Container>
        <Title>Token contract</Title>
        <Pre>
          <Code>{token.source}</Code>
        </Pre>
        <Title>Sale contract</Title>
        <Pre>
          <Code>{saleContract.source}</Code>
        </Pre>
      </Container>
    );
  }
}

export default Deploy;