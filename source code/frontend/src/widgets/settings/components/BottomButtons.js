import React, { Component } from "react";
import styled from "styled-components";
import Button from "./Button";

const Container = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

class BottomButtons extends Component {
  render() {
    const { disabledButtons = {}, onNextClick, onBackClick, nextButtonName = 'Continue' } = this.props;
    return (
      <Container>
        <Button disabled={!!disabledButtons.back} onClick={onBackClick}>
          Back
        </Button>
        <Button disabled={!!disabledButtons.continue} onClick={onNextClick}>
          {nextButtonName}
        </Button>
      </Container>
    );
  }
}

export default BottomButtons;
