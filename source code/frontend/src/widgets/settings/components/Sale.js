import React, { Component } from "react";
import styled from "styled-components";
import AInput from "../../../core/Input";
import ASelect from "../../../core/Select";

const Container = styled.div``;

const LabelWrap = styled.div`
  position: relative;
  width: calc(50% - 10px);
`;

const Label = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  opacity: 0.5;
`;

const Block = styled.div`
  width: 100%;
  margin-bottom: 30px;
`;

const Title = styled.div`
  font-size: 20px;
  margin-bottom: 10px;
`;

const Subtitle = styled.div`
  margin-bottom: 20px;
`;

const InputsWrap = styled.div`
  display: flex;
  flex-direction: row;
`;

const Input = styled(AInput)`
  width: ${p => p.widthPercent};
`;

const FirstInput = styled(Input)`
  margin-right: 20px;
`;

const Select = styled(ASelect)`
  width: calc(50% - 50px);
  margin-right: 20px;
`;

class Sale extends Component {
  render() {
    const { values, onChange } = this.props;
    return (
      <Container>
        <Block>
          <Input
            widthPercent="100%"
            value={values.saleName}
            onChange={event => onChange("saleName", event.target.value)}
            hintText="Pre ICO, Public Sale, ICO etc."
          />
        </Block>
        <Block>
          <Title>Sale period</Title>
          <Subtitle />
          <InputsWrap>
            <LabelWrap>
              <Label>Start (required)</Label>
              <FirstInput
                widthPercent="50%"
                value={values.startSale}
                onChange={event => onChange("startSale", event.target.value)}
                hintText="Time in UTC timezone."
                placeholder="dd/mm/yyyy, HH:MM"
              />
            </LabelWrap>
            <LabelWrap>
              <Label>End</Label>
              <Input
                widthPercent="50%"
                value={values.endSale}
                onChange={event => onChange("endSale", event.target.value)}
                hintText="Time in UTC timezone."
                placeholder="dd/mm/yyyy, HH:MM"
              />
            </LabelWrap>
          </InputsWrap>
        </Block>
        <Block>
          <Title>Sale cap</Title>
          <Subtitle>Do not specify if you need eternal sale</Subtitle>
          <InputsWrap>
            <Input
              widthPercent="100%"
              value={values.saleCap}
              onChange={event => onChange("saleCap", event.target.value)}
            />
          </InputsWrap>
        </Block>
        <Block>
          <Title>Sale rates</Title>
          <Subtitle />
          <InputsWrap>
            <Select
              disabled
              options={[{ value: "QKC", label: "QKC" }]}
              onChange={event => onChange("saleRatesType", event)}
              value={values.saleRatesType}
            />
            <Input
              widthPercent="calc(50% - 10px)"
              value={values.saleRatesAmount}
              onChange={event =>
                onChange("saleRatesAmount", event.target.value)
              }
              hintText="Amount of tokens buyer will get for 1 coin"
            />
          </InputsWrap>
        </Block>
      </Container>
    );
  }
}

export default Sale;
