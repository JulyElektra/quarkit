import React, { Component } from "react";
import styled from "styled-components";
import Step from "../components/Step";
import stepsMap from "../config/stepsMap";

const Container = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin-bottom: 30px;
`;

class Steps extends Component {
  render() {
    return (
      <Container>
        {stepsMap.map(item => (
          <Step
            key={item.index}
            index={item.index}
            title={item.title}
            currentStep={this.props.currentStep}
          />
        ))}
      </Container>
    );
  }
}

export default Steps;
