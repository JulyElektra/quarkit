import React, { Component } from "react";
import styled from "styled-components";

const Container = styled.div`
  display: flex;
  flex-direction: row;

  ${p => p.hidden && "opacity: 0.5;"};
`;

const Index = styled.span`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 20px;
  height: 20px;
  margin-right: 10px;
  border-radius: 50%;
  background-color: #1c98fb;
  color: #fff;
`;

const Text = styled.div`
  padding-top: 2px;
`;

class Step extends Component {
  render() {
    const { index, title, currentStep } = this.props;
    return (
      <Container hidden={index > currentStep}>
        <Index>{index}</Index> <Text>{title}</Text>
      </Container>
    );
  }
}

export default Step;
