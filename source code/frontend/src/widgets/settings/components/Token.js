import React, { Component } from "react";
import styled from "styled-components";
import AInput from "../../../core/Input";
import Checkbox from "../../../core/Checkbox";

const Container = styled.div`
  margin-bottom: 40px;
`;

const InputsWrap = styled.div`
  display: flex;
  flex-direction: row;
  margin-bottom: 10px;
`;

const Input = styled(AInput)`
  width: calc(50% - 10px);
`;

const LowInput = styled(AInput)`
  width: calc(33% - 10px);
  margin-right: 20px;
`;

const WideInput = styled(Input)`
  margin-right: 20px;
`;

const CheckboxWrap = styled.div`
  display: flex;
  width: 100%;
  max-width: 33%;
  align-items: center;
  cursor: pointer;
`;

class Token extends Component {
  render() {
    const { onChange, values } = this.props;
    return (
      <Container>
        <InputsWrap>
          <WideInput
            placeholder="Token Name (required)"
            value={values.tokenName}
            onChange={event => onChange("tokenName", event.target.value)}
            hintText="The name of your token. It will be visible in Etherscan."
          />
          <Input
            placeholder="Token Symbol (required)"
            value={values.tokenSymbol}
            onChange={event => onChange("tokenSymbol", event.target.value)}
            hintText="The token symbol is an abbreviation for your token. It will be visible in Etherscan and used as a ticker symbol."
          />
        </InputsWrap>
        <InputsWrap>
          <LowInput
            placeholder="Decimals (required)"
            value={values.decimals}
            onChange={event => onChange("decimals", event.target.value)}
            hintText="The ability to divide your token into pieces. 18 refers to 10^18 pieces for each token."
          />
          <CheckboxWrap onClick={() => onChange("pausable", !values.pausable)}>
            <Checkbox checked={values.pausable} onChange={Function.prototype}>
              Pausable
            </Checkbox>
          </CheckboxWrap>

          <CheckboxWrap onClick={() => onChange("burnable", !values.burnable)}>
            <Checkbox checked={values.burnable} onChange={Function.prototype}>
              Burnable
            </Checkbox>
          </CheckboxWrap>
        </InputsWrap>
      </Container>
    );
  }
}

export default Token;
