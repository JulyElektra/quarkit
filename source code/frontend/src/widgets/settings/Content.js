import React, { Component } from "react";
import styled from "styled-components";
import Container from "../../core/Container";
import Steps from "./components/Steps";
import BottomButtons from "./components/BottomButtons";

import Token from "./components/Token";
import Sale from "./components/Sale";
import Compliance from "./components/Compliance";
import Marketing from "./components/Marketing";
import Deploy from "./components/Deploy";

import { deploy } from "../quarkchain/Content";

const Title = styled.div`
  font-size: 24px;
  margin-bottom: 30px;
`;

class Content extends Component {
  state = {
    currentStep: 1,
    disabledButtons: {
      back: false,
      continue: false
    },
    // step 1
    tokenName: "",
    tokenSymbol: "",
    decimals: "",
    pausable: true,
    burnable: false,
    // step 2
    saleName: "Public Sale",
    startSale: "",
    endSale: "",
    saleCap: "",
    saleRatesType: { value: "QKC", label: "QKC" },
    saleRatesAmount: 0,
    // step 3
    tokenContract: "",
    saleContract: ""
  };

  disabledButtonsCheck() {
    const { currentStep } = this.state;
    let disabledButtons = {
      back: false,
      continue: false
    };

    if (currentStep === 1) {
      disabledButtons.back = true;
      const { tokenName, tokenSymbol, decimals } = this.state;
      if (!tokenName || !tokenSymbol || (isNaN(+decimals) || decimals <= 0))
        disabledButtons.continue = true;
    }

    if (currentStep === 2) {
      const {
        saleName,
        startSale,
        endSale,
        saleCap,
        saleRatesType,
        saleRatesAmount
      } = this.state;
      if (
        !saleName ||
        !startSale ||
        !endSale ||
        !saleCap ||
        !saleRatesType ||
        !saleRatesAmount ||
        saleRatesAmount <= 0
      )
        disabledButtons.continue = true;
    }

    return disabledButtons;
  }

  componentDidMount() {
    this.disabledButtonsCheck();
  }

  render() {
    const { currentStep, disabledButtons, pausable, burnable } = this.state;

    let currentView;

    switch (this.state.currentStep) {
      case 1:
        currentView = (
          <Token
            values={this.state}
            onChange={(field, value) => this.setState({ [field]: value })}
          />
        );
        break;

      case 2:
        currentView = (
          <Sale
            values={this.state}
            onChange={(field, value) => this.setState({ [field]: value })}
          />
        );
        break;

      case 3:
        currentView = <Deploy values={this.state} {...this.props}/>;
        break;

      default:
        currentView = null;
    }

    return (
      <Container>
        <Title>Launch Crowdsale</Title>
        <Steps currentStep={currentStep} />
        {currentView}
        <BottomButtons
          disabledButtons={this.disabledButtonsCheck()}
          onBackClick={() => this.setState({ currentStep: currentStep - 1 })}
          nextButtonName={currentStep === 3 ? "Deploy" : "Continue"}
          onNextClick={
            currentStep === 3
              ? () => deploy({pausable, burnable}, this.props)
              : () => this.setState({ currentStep: currentStep + 1 })
          }
        />
      </Container>
    );
  }
}

export default Content;
