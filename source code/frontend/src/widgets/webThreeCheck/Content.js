import React, { Component } from "react";
import Container from '../../core/Container';

class Content extends Component {
  render() {
    return (
      <Container>
        Cannot find Web3, please use metamask or something similar to be able to
        communicate with Ethereum network from your browser
      </Container>
    );
  }
}

export default Content;
