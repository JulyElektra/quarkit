import React, { Component } from "react";
import Container from "../core/Container";

class App extends Component {
  componentWillMount() {
    if (!this.props.match.params.crowdsaleId)
      this.props.history.push("/settings");
    else if (!this.props.user) {
      this.props.history.push("/auth");
    }
  }

  render() {
    return <Container>app</Container>;
  }
}

export default App;
