import React, { Component } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Header from "./widgets/header/Content";
import Main from "./widgets/App";
import Auth from "./widgets/auth/Content";
import Settings from "./widgets/settings/Content";
import WebThreeCheck from "./widgets/webThreeCheck/Content";

class App extends Component {
  state = {
    user: null
  };

  render() {
    const WrappedAuth = function(props) {
      return (
        <Auth
          {...props}
          user={this.state.user}
          setUser={user => this.setState({ user })}
        />
      );
    }.bind(this);

    return (
      <div className="App">
        <Header />
        <BrowserRouter>
          <Switch>
            <Route path="/:crowdsaleId?/auth" component={WrappedAuth} />
            <Route path="/settings" component={Settings} />
            <Route exact path="/:crowdsaleId?" component={Main} />
          </Switch>
        </BrowserRouter>
        {!window.web3 && <WebThreeCheck />}
      </div>
    );
  }
}

export default App;
