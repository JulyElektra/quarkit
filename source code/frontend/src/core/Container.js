import React, { Component } from "react";
import styled from "styled-components";

const Container = styled.div`
  width: 100%;
  max-width: 1000px;
  padding: 30px 40px;
  margin: 0 auto 20px;
  background-color: #fff;
  border-radius: 10px;
`;

class Page extends Component {
  render() {
    return (
      <Container className={this.props.className}>
        {this.props.children}
      </Container>
    );
  }
}

export default Page;
