import React, { Component } from "react";
import ReactSelect from "react-select";

export default class Select extends Component {
  render() {
    const { placeholder, options, value, className, onChange } = this.props;
    return (
      <div className={className}>
        <ReactSelect
          placeholder={placeholder}
          options={options}
          value={value}
          clearable={false}
          searchable={false}
          onChange={onChange}
        />
      </div>
    );
  }
}
