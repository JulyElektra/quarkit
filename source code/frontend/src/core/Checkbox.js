import React, { Component } from "react";
import styled from 'styled-components';

const Text = styled.span`
`;

class Checkbox extends Component {
  render() {
    const { children, ...rest } = this.props;
    return (
      <div>
        <input type="checkbox" {...rest} />
        <Text>{this.props.children}</Text>
      </div>
    );
  }
}

export default Checkbox;
