pragma solidity ^0.4.21;

import "./TokenImpl.sol";
import "./PausableToken.sol";
import "@daonomic/util/contracts/OwnableImpl.sol";

contract PausableTokenImpl is TokenImpl, PausableToken, OwnableImpl {
    
    constructor(address _owner) public {
        owner = _owner;
    }
}