pragma solidity ^0.4.21;

import "./TokenImpl.sol";
import "./BurnableTokenImpl.sol";
import "./PausableToken.sol";
import "@daonomic/util/contracts/OwnableImpl.sol";

contract PausableBurnableTokenImpl is TokenImpl, PausableToken, BurnableTokenImpl, OwnableImpl {

    constructor(address _owner) public {
        owner = _owner;
    }
}