pragma solidity ^0.4.21;

import "openzeppelin-solidity/contracts/crowdsale/Crowdsale.sol";
import "openzeppelin-solidity/contracts/crowdsale/emission/MintedCrowdsale.sol";
import "openzeppelin-solidity/contracts/crowdsale/validation/CappedCrowdsale.sol";
import "openzeppelin-solidity/contracts/crowdsale/validation/TimedCrowdsale.sol";

import "openzeppelin-solidity/contracts/token/ERC20/ERC20.sol";

/// A crowdsale contract based on open-zeppelin
contract CrowdsaleImpl is Crowdsale, CappedCrowdsale, MintedCrowdsale, TimedCrowdsale {

    /// _startTime - ICO start date
    /// _endTime - ICO end date
    /// _rate - exchnge rate from QCK to our token
    /// _wallet - wallet to transfer QCK to
    /// _token - token contract address
    constructor(uint256 _startTime, uint256 _endTime, uint256 _rate, uint256 _cap, address _wallet, address _token) public
        CappedCrowdsale(_cap)
        Crowdsale(_rate, _wallet, ERC20(_token))
        TimedCrowdsale(_startTime, _endTime)
    {
    }
}