# Quarkit

ICO Framework on QuarkChain in sharded way, including UI (ex. smart contract generation by one-click)

## Run a Docker Image

To read the container registry images, you'll need to install Docker.

Then run command: 

docker load --input quarkit.tar

docker images

docker run -i -t -p 3000:3000 [image id]


## Getting Started

Using a browser with MetaMask (Authorization)

Open http://localhost:3000

Step Token

Enter Token Name (required)

Enter Token Symbol (required)

Enter Decimals (required)

Checking Pausable (option)

Checking Burnable (option)

Press Continue


Step Sale

Enter Sale period (required)

Enter Sale cap (required)

Enter Amount of tokens for 1 QKC(required)

Press Continue


Step Review and deploy

Preview Solidity code for Token and Sale contracts

Press Deploy

Sign transaction with Metamask

See dialog with address token


Open Testnet and make sure that token is deployed

## Report

Use off-chain deploy because on-chain not work due to a bug in EVM QuarkChain.

We have reported this problem to Quarkchain team.

## Contact

Team member: Vasin Denis

mailto: akafakir@gmail.com